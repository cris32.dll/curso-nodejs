// DEPENDENCIAS
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const express = require("express");
const flash = require("express-flash");
const session = require("express-session");


// CONFIGURACOES
var app = express();

// seta o ejs para ser a view engine
app.set('view engine', 'ejs');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());

// é importante que a configuraçao do cookie-parser seja antes da session
const secret = "VaChuparUmCanavialDeRolaDepoisDeDarMeiaHoraDeCu";
app.use(cookieParser(secret));

// configuraçao do express session
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}));

// configura o express-flash
app.use(flash());

// ROTAS

/*Template de rotas
app.get("/", (req, res) => {
    res.render("index");
});
*/


app.get("/", (req, res) => {
    var errorEmail = req.flash("errorEmail");
    var errorNome = req.flash("errorNome");
    var errorPontos = req.flash("errorPontos");

    var email= req.flash("email");

    // Se o flash não encontrar o parâmentro, ele retorna um array vazio.
    // Isto pode ser tratado da seguinte forma
    errorEmail = (errorEmail == undefined || errorEmail.length == 0) ? undefined : errorEmail;
    errorNome = (errorNome == undefined || errorNome.length == 0) ? undefined : errorNome;
    errorPontos = (errorPontos == undefined || errorPontos.length == 0) ? undefined : errorPontos;

    email = (email == undefined || email.length == 0) ? undefined : email;
 
    //O ultimo campo é para não pertder as informações do form
    res.render("index", {errorEmail, errorNome, errorPontos, email});
});


app.post("/form", (req, res) => {
    var {email, nome, pontos} = req.body;

    var errorEmail;
    var errorNome;
    var errorPontos;
    
    if(email == undefined || email == ""){
        errorEmail = "Erro no email";
    }

    if(pontos == undefined || pontos < 20){
        errorPontos = "Erro no ponto";
    }

    if(nome == undefined || nome == ""){
        errorNome = "Erro no nome";
    }

    if(errorNome != undefined || errorEmail != undefined || errorPontos != undefined) {
        //O Express flash é usado para criar uma sessao entre rotas
        req.flash("errorNome", errorNome);
        req.flash("errorEmail", errorEmail);
        req.flash("errorPontos", errorPontos);

        //Para retornar o erro sem limpar o formulário. Ex. feito só com email
        req.flash("email", email);
        res.redirect("/");
    }else{
        res.send("Got it!");
    }
});


// RODA A APLICACAO
app.listen(5678, (req, reas) => {
    console.log("Servidor rodando...");
});