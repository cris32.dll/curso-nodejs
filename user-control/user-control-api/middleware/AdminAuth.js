var jwt = require("jsonwebtoken");

//Precisa ser o mesmo usado na criação. É recomendado deixar isso em um arquivo separado.
var secret = "vashuparumcanavialderoladepoisdedarmeiahoradecu";

module.exports = function(req, res, next){
    const authToken = req.headers['authorization']

    if(authToken != undefined){
        const bearer = authToken.split(' ');
        var token = bearer[1];

        try{
            var decoded = jwt.verify(token,secret);
            
            //1 = adm neste exemplo
            if(decoded.role == 1){
                next();
            }else{
                res.status(403);
                res.send("Você não tem permissão para isso!");
                return;
            }
        }catch(error){
            res.status(403);
            res.send("Você não está autenticado");
            return;
        }
    }else{
        res.status(403);
        res.send("Você não está autenticado");
        return;
    }
}