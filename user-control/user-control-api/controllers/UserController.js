var User = require("../models/User");
var PasswordToken = require("../models/PasswordToken");
var jwt = require("jsonwebtoken");
var secret = "vashuparumcanavialderoladepoisdedarmeiahoradecu";
var bcrypt = require("bcrypt");

class UserController{

    async index(req, res){
        var users = await User.findAll();
        res.json(users);
    }

    async findUser(req, res){
        var id = req.params.id;
        var user = await User.findById(id);
        if(user == undefined){
            res.status(404);
            res.json({});
        }else{
            res.status(200)
            res.json(user);
        }
    }

    //O nome deste método pode ser qualquer coisa
    async create(req, res){
        //criação das variáveis usando o Destructuring do Javascript
        var {name, email, password, role} = req.body;

        if(email == undefined || email == '' || email == ' '){
            res.status(400);
            res.json({err: "E-mail inválido!"});
            return;
        }

        var emailAlredyExists = await User.findEmail(email);

        if(emailAlredyExists){
            res.status(406);
            res.json({err: "Este email já possui cadastro!"});
            return;
        }

        await User.new(name, email, password, role);

        res.status(200);
        res.send("Tudo Azul!");
    }

    async edit(req, res){
        //Atanção. Quando for testar, verifique se esta ordem influencia no update
        var {id, name, role, email} = req.body;
        var result = await User.update(id,email,name,role);
        if(result != undefined){
            if(result.status){
                res.status(200);
                res.send("Tudo OK!");
            }else{
                res.status(406);
                res.send(result.err)
            }
        }else{
            res.status(406);
            res.send("Ocorreu um erro no servidor!");
        }
    }

    async remove(req, res){
        var id = req.params.id;

        var result = await User.delete(id);

        if(result.status){
            res.status(200);
            res.send("Tudo OK!");
        }else{
            res.status(406);
            res.send(result.err);
        }
    }

    async recoverPassword(req, res){
        var email = req.body.email;
        var result = await PasswordToken.create(email);
        if(result.status){
           res.status(200);
           //O Send não funfa bem com número. Por isso estou concatenando com string vazia para transformar em string... Gambiarra.
           res.send("" + result.token);
        }else{
            res.status(406)
            res.send(result.err);
        }
    }

    async changePassword(req, res){
        var token = req.body.token;
        var password = req.body.password;
        var isValidToken = await PasswordToken.validate(token);
        if(isValidToken.status){
            await User.changePassword(password, isValidToken.token.user_id, isValidToken.token.token);
            res.status(200);
            res.send("Senha alterada");
        }else{
            res.status(406);
            res.send("Token inválido!");
        }
    }

    async login(req, res){
        var {email, password } = req.body;

        var user = await User.findByEmail(email);

        if(user != undefined){

            var resultado = await bcrypt.compare(password,user.password);

            if(resultado){

                var token = jwt.sign({ email: user.email, role: user.role }, secret);

                res.status(200);
                res.json({token: token});

            }else{
                res.status(406);
                res.json({err: "Senha Incorreta"});
            }

        }else{
            res.status(406);
            res.json({status: false, err: "O Usuário não existe"});

        }
    }



}

module.exports = new UserController();