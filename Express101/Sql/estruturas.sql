CREATE TABLE usuarios(
    nome VARCHAR(50),
    email VARCHAR(100),
    idade INT
);

INSERT INTO usuarios(nome, email, idade) VALUES("Cristiano Vieira", "cris32.dll@gmail.com", 38);
INSERT INTO usuarios(nome, email, idade) VALUES("Jonny De Bruce", "creakazoid@yahoo.com.br", 38);
INSERT INTO usuarios(nome, email, idade) VALUES("Zoid", "vieira.10envolvedor@outlook.com", 38);

SELECT * FROM usuarios;

DELETE FROM usuarios WHERE nome = "Zoid";

UPDATE usuarios SET idade = 50 WHERE nome = "Jonny De Bruce";


