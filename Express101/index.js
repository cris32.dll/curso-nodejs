const express = require("express"); // Inporta a biblioteca como um módulo.
const app = express();              // Cria uma instancia da biblioteca importada.   

// Cria uma rota para o inicio da aplicação
app.get("/", function(req, res){
    res.send("<h1>Aqui deve ser implementado a página inicial</h1>");
});

// Cria uma rota para o blog
app.get("/blog", function(req, res){
    res.send("Aqui deve ser implementado o blog.");
});

// Cria uma rota para o player de vídeo
app.get("/youtube", function(req, res){
    var canal = req.query["canal"]; //Utilizando Query params - /youtube?nomeParam=valorParam

    if(canal){
        res.send("Aqui deve ser implementado o player de vídeo do canal." + canal);
    }else{
        res.send("Aqui deve ser implementado o player de vídeo.");
    }
});

// Cria uma rota para testar passagem de parametros
app.get("/calcula/:operacao/:operando1/:operando2?", function(req, res){
    //req => Dados enviados pelo usuário
    //res => resposta que vai ser dada ao usuário
    //? => server pra transformar o parametro em nao obrigatório
    let resultado;
    if(req.params.operacao == "soma"){
        resultado = parseInt(req.params.operando1) + parseInt(req.params.operando2);
        res.send(resultado);
    }
    if(req.params.operacao == "sub"){
        resultado = req.params.operando1 - req.params.operando2;
        res.send(resultado);
    }
    if(req.params.operacao == "mult"){
        resultado = req.params.operando1 * req.params.operando2;
        res.send(resultado);
    }
    if(req.params.operacao == "div"){
        if(req.params.operando2){
            if(req.params.operando2 != 0){
                resultado = req.params.operando1 / req.params.operando2;
                res.send(resultado);    
            }else{
                res.send("Resultado indeterminado.");    
            }    
        }{
            res.send("Operaçao nessecita de segundo parametro.");    
        }
    }
});


// Inicia um servidor web na porta 4000
app.listen(4000, function(erro){
    if(erro){
        console.log("Deu erro, seu burro!");
    }else{
        console.log("Esse é o meu garoto!")
    }
})