const fs = require("fs");
const util = require("util");

class Writer{
    constructor(){
        //Converte a função readFile muma promessa
        this.writerPromise = util.promisify(fs.writeFile);
    }

    async Write(filename, data){
        try {
            await this.writerPromise(filename, data);
            return true;
        } catch (error) {
            return false;            
        }
    }
}

module.exports = Writer;