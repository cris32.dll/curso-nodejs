var Reader = require("./Reader");
var Processor = require("./Processor");
var Table = require("./Table");
var HtmlParser = require("./HtmlParser");
var Writer = require("./Writer");
var PDFWriter = require("./PDFWriter");

var csvReader = new Reader();

async function main(){
    var dadosCSV = await csvReader.Read("./users.csv");
    console.log(dadosCSV);
    console.log("-------------------------");
    var processedData = Processor.Process(dadosCSV);
    console.log(processedData);
    console.log("-------------------------");
    var user = new Table(processedData);

    console.log(user.header);
    console.log("-------------------------");
    console.log(user.rows);
    console.log("-------------------------");
    console.log(user.RowsCount);
    console.log(user.ColumnCount);
    console.log("-------------------------");
    var html = await HtmlParser.Parse(user);
    console.log(html);
    console.log("==========================");
    var escritor = new Writer();
    if(escritor.Write("UsuariosConvertido.html", html)){
        console.log("Deu certo");
    }else{
        console.log("Deu ruim");
    }
    console.log("==========================");
    PDFWriter.Write("UsuariosConvertido.pdf", html);

}

main();
