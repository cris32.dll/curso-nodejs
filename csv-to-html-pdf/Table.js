class Table{
    constructor(arr){
        this.header = arr[0];
        arr.shift(); // Remove o primeiro elemento do array
        this.rows = arr;
    }

    //O get transforma o método em campo virtual, que pode ser chamado como atributo
    get RowsCount(){
        return this.rows.length;
    }

    get ColumnCount(){
        return this.header.length;
    }
}

module.exports = Table;