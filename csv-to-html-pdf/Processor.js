class Processor{
    static Process(data){
        var vetData = data.split("\n");
        var rows = [];

        vetData.forEach(row => {
            var vetRow = row.split(",");
            rows.push(vetRow);
        });

        return(rows);
    }
}

module.exports = Processor;