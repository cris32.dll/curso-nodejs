const fs = require("fs");
const util = require("util");

class Reader{
    constructor(){
        //Converte a função readFile muma promessa
        this.readerPromise = util.promisify(fs.readFile);
    }

    //Desta fomrma agora é possível retornar os dados
    async Read(filepath){
        try {
            return await this.readerPromise(filepath, {encoding: "utf-8"});
        } catch (error) {
            return undefined;            
        }
    };
};

module.exports = Reader;