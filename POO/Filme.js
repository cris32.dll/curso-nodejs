//Definição de classe
class Filme{
    constructor(titulo, ano, genero, diretor, atores, duracao){
        this.titulo = titulo;
        this.ano = ano;
        this.genero = genero;
        this.diretor = diretor;
        this.atores = atores;
        this.duracao = duracao;
    }

    Reproduzir(){
        console.log("Reproduzindo...");
    }

    Pausar(){
        console.log("Pausado ||");
    }

    Avancar(){
        console.log("Avançar >>");
    }

    Fechar(){
        console.log("Fechar X");
    }
}

//Classe com métodos estáticos
class Calculadora{
    static Somar(a, b){
        return(a+b);
    }

    static Subtrair(a, b){
        return(a-b);
    }

}


//Criação de Objetos
var vingadores = new Filme();
var hulk = new Filme();
var starwars = new Filme();

//Uso de métodos estáticos
console.log(Calculadora.Somar(1, 2));
console.log(Calculadora.Subtrairomar(8, 2));