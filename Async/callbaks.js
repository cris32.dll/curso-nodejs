function enviarEmail(pBody, pTo, pCallback){
    setTimeout(() => {
        console.log(`
            Para: ${pTo}
            ----------------------------------
            ${pBody}
            ----------------------------------
            De: Creakazoid!
        `);
        pCallback("OK", 5, "8s");
    }, 8000);
};

console.log("Envio de email iniciado");
enviarEmail("De bukis on de teibou", "a@b.com", (pStatus, pAmount, pTime) => {
    console.log("Envio do email concluído!");
    console.log(`
        ----------------------------------
        Status: ${pStatus}
        ----------------------------------
        Contratos: ${pAmount}
        ----------------------------------
        Tempo: ${pTime}
        ----------------------------------
    `);
});
console.log("Seu email foi enviado.");
console.log("tudo azul!");