function pegarId(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(5);
        }, 1500);
    });
};

function buscarEmailNoBanco(pId){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("cris32.dll@gmail.com");
        }, 2000);
    });
};

function enviarEmail(pBody, pTo){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            var deuRuim = false;

            if(!deuRuim){
                resolve({qtd: 15, cargo: "Gerentes"}); //A Promessa será cumprida
            }else{
                reject("Seu burro!!!"); //Não deu
            }
        }, 4000);
    });
};

function pegarUsuarios(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
                resolve([
                    {name: "Cristiano", lang: "C++"},
                    {name: "Nascimento", lang: "ASM"},
                    {name: "Vieira", lang: "Node"},
                ]);
        }, 4000);
    });
};

async function principal(){
    var vId = await pegarId(); 
    var vEmail = await buscarEmailNoBanco(vId);

    try {
        await enviarEmail("De bukis on de teibou", vEmail);
        console.log("Email enviado \o/")
    } catch (error) {
        console.log("Fo-deu!");
    }
};

console.log("Preparando email para envio...");
principal();
