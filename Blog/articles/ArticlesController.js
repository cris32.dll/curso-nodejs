const Express = require("express");
const Router = Express.Router();
const Category = require("../categories/Category");
const Article = require("./Article");
const Slugify = require("slugify");
const AdminAuth = require("../middlewares/adminAuth");

Router.get("/admin/articles", AdminAuth, (req, res) => {
    Article.findAll({
        include: [{model: Category}]
    }).then(a => {
        res.render("admin/articles/index", {articles: a});
    });
});

Router.get("/admin/articles/new", AdminAuth, (req, res) => {
    Category.findAll().then(c => {
        res.render("admin/articles/new", {categories: c});
    });
});

Router.post("/articles/save", AdminAuth, (req, res) => {
    var vTitle = req.body.title;
    var vBodyCategory = req.body.bodyCategory;
    var vCaegoryId = req.body.categorySelected;

    Article.create({
        title: vTitle,
        slug: Slugify(vTitle),
        body: vBodyCategory,
        categoryId: vCaegoryId
    }).then(() => {
        res.redirect("/admin/articles");
    })

});

Router.post("/articles/delete", AdminAuth, (req, res) => {
    var vId = req.body.idArticle;

    if(vId != undefined){
        if(!isNaN(vId)){
            Article.destroy({
                where: {
                    id: vId
                }
            }).then(() => {
                res.redirect("/admin/articles");
            });
        }else{
            res.redirect("/admin/articles"); // Se id não for um número
        }
    }else{
        res.redirect("/admin/articles"); // Se id for nulo
    }
});

Router.get("/admin/articles/edit/:pId", AdminAuth, (req, res) => {
    var vId = req.params.pId;
    Article.findByPk(vId).then(a => {
        if(a != undefined){
            Category.findAll().then(c => {
                res.render("admin/articles/edit", {article: a, categories: c})
            })
        }else{
            res.redirect("/");
        }
    }).catch(err => {
        res.redirect("/");
    })
});

Router.post("/articles/update", AdminAuth, (req, res) => {
    var vId = req.body.idArticle;
    var vTitle = req.body.titArticle;
    var vBody = req.body.bodyArticle;
    var vCategory = req.body.categorySelected;

    Article.update({
        title: vTitle,
        body: vBody,
        categoryId: vCategory,
        slug: Slugify(vTitle)
    }, {
        where: {
            id: vId
        }
    }).then(() => {
        res.redirect("/admin/articles");
    }).catch(err => {
        res.redirect("/");
    });
});

Router.get("/articles/page/:pNum", (req, res) => {
    var vPage = req.params.pNum;
    var vOffset = 0;
    var vArticlePerPage = 4;

    if(isNaN(vPage) || vPage == 1) {
        vOffset = 0;
    }else{
        vOffset = (parseInt(vPage) - 1) * vArticlePerPage;
    }

    Article.findAndCountAll({
        limit: vArticlePerPage,
        offset: vOffset,
        order: [
            ['id', 'DESC']
        ]
    }).then(a => {

        var vNext;
        if(vOffset + vArticlePerPage >= a.count){
            vNext = false;
        }else{
            vNext = true;
        };

        var vResult = {
            page: parseInt(vPage),
            next: vNext,
            articles: a
        };

        Category.findAll().then(c => {
            res.render("admin/articles/page", {result: vResult, categories: c});
        });
    });
});

module.exports = Router;