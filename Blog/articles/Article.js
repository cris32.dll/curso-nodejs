const Sequelize = require("sequelize");
const dbConn = require("../database/database");
const Category = require("../categories/Category");

// DEFINIÇÃO DO MODEL DE ARTIGOS 
const Article = dbConn.define('articles',{
    title:{
        type: Sequelize.STRING,
        allowNull: false
    },
    slug:{
        type: Sequelize.STRING,
        allowNull: false
    },
    body:{
        type: Sequelize.TEXT,
        allowNull: false
    }
});

// DEFINIÇÃO DOS RELACIONAMENTOS

Category.hasMany(Article); //Um para muitos
Article.belongsTo(Category); // Um para um

// EXPORTAÇÃO

module.exports = Article;