// DEPENDÊNCIAS
const Sequelize = require("sequelize");

// CONEXÃO
const conn = new Sequelize('Blog', 'root', '11235813', {
    host: 'localhost',
    dialect: 'mysql',
    timezone: "-03:00"
});

module.exports = conn;