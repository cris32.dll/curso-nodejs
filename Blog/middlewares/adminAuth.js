function adminAuth(req, res, next) {
    if(req.session.userLogged != undefined) {
        next();
    }else{
        res.redirect("/login");   
    }
}

module.exports = adminAuth;