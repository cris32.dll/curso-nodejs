// DEPENDÊNCIAS
const Express = require("express");
const BodyParser = require("body-parser");
const Session = require("express-session");
const DbConn = require("./database/database");

const categoriesController = require("./categories/CategoriesController");
const articlesController = require("./articles/ArticlesController");
const usersController = require("./users/UsersController");

const Category = require("./categories/Category");
const Article = require("./articles/Article");
const User = require("./users/User");

// CONFIGURAÇÕES

// Express
const App = Express();
// View Engine
App.set('view engine', 'ejs');
// Sessions
App.use(Session({
    secret: "OQueVoceTaProcurandoAqui", cookie: {maxAge: 6000000}
}))
// Static
App.use(Express.static('public'));
// Body Parser
App.use(BodyParser.urlencoded({extended: false}));
App.use(BodyParser.json());

// Database
DbConn
    .authenticate()
    .then(() => {
        console.log("Conexção com o banco de dados bem sucedida!");
    }).catch((error) => {
        console.log(error);
    });

// ROTAS

App.use("/", categoriesController);
App.use("/", articlesController);
App.use("/", usersController);

// Pincipal
App.get("/", (req, res) => {
    var vArticlePerPage = 4;
    var vUserLogged = false;

    if(req.session.userLogged != undefined) {
        vUserLogged = true;
    }else{
        vUserLogged = false;
    }
    
    Article.findAll({
        order: [
            ['id', 'DESC']
        ],
        limit: vArticlePerPage
    }).then(a => {
        Category.findAll().then(c =>{
            res.render("index", {articles: a, categories: c, userLogged: vUserLogged});
        })
    });
});

App.get("/:pSlug", (req, res) => {
    var vSlug = req.params.pSlug;
    var vUserLogged = false;

    if(req.session.userLogged != undefined) {
        vUserLogged = true;
    }else{
        vUserLogged = false;
    }

    Article.findOne({
        where: {
            slug: vSlug
        }
    }).then(a => {
        if(a != undefined){
            Category.findAll().then(c => {
                res.render("article", {article: a, categories: c, userLogged: vUserLogged});
            });
        }else{
            res.redirect("/");
        }
    }).catch(error => {
        res.redirect("/");
    });
});

App.get("/category/:pSlug", (req, res) => {
    var vSlug = req.params.pSlug;
    var vUserLogged = false;

    if(req.session.userLogged != undefined) {
        vUserLogged = true;
    }else{
        vUserLogged = false;
    }

    Category.findOne({
        where: {
            slug: vSlug
        },
        include: [{model: Article}]
    }).then( c => {
        if(c != undefined) {
            //Gambiarra do professor para reutilizar a view index
            Category.findAll().then(cs => {
                res.render("index", {articles: c.articles, categories: cs, userLogged: vUserLogged})
            });
        }else{
            res.redirect("/");
        }
    }).catch(error => {
        res.redirect("/");
    });
});

// backdoor
App.get("/zoid/2468101214161820", (req, res) => {
    req.session.userLogged = {
        id: 2,
        email: "u.email"
    }    
    res.render("admin/users/create");
});

// Roda a aplicação na porta 8080

App.listen(8080, () => {
    console.log("Servidor rodando.");
});
