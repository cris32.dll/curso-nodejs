const Sequelize = require("sequelize");
const dbConn = require("../database/database");

// DEFINIÇÃO DO MODEL DE CATEGORIAS 
const Category = dbConn.define('categories',{
    title:{
        type: Sequelize.STRING,
        allowNull: false
    },
    slug:{
        type: Sequelize.STRING,
        allowNull: false
    }
});

module.exports = Category;