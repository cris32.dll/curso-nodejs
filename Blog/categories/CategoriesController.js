const Express = require("express");
const Router = Express.Router();
const Category = require("./Category");
const Slugify = require("slugify");
const AdminAuth = require("../middlewares/adminAuth");

Router.get("/admin/categories/new", AdminAuth, (req, res) => {
    res.render("admin/categories/new");
});

Router.post("/categories/save", AdminAuth, (req, res) => {
    var title = req.body.title;
    if(title != undefined){

        Category.create({
            title: title,
            slug: Slugify(title)
        }).then(() => {
            res.redirect("/admin/categories");
        })
    }else{
        res.redirect("/admin/categories/new");
    }
});

Router.get("/admin/categories", AdminAuth, (req, res) => {
    Category.findAll().then(c => {
        res.render("admin/categories/index", {categories: c});
    });
});

Router.post("/categories/delete", AdminAuth, (req, res) => {
    var vId = req.body.idCategory;

    if(vId != undefined){
        if(!isNaN(vId)){
            Category.destroy({
                where: {
                    id: vId
                }
            }).then(() => {
                res.redirect("/admin/categories");
            });
        }else{
            res.redirect("/admin/categories"); // Se id não for um número
        }
    }else{
        res.redirect("/admin/categories"); // Se id for nulo
    }
});

Router.get("/admin/categories/edit/:pId", AdminAuth, (req, res) =>{
    var vId = req.params.pId;

    if(isNaN(vId)){ //Verifica se o parâmetro é um número. Isso é pra corrigir um bug. Se passar um id seguido de letras, o sistema aceita.
        res.redirect("/admin/categories");
    }

    Category.findByPk(vId).then(c => {
        if(c != undefined){
            res.render("admin/categories/edit",{category: c});
        }else{
            res.redirect("/admin/categories");
        }
    }).catch(error => {
        res.redirect("/admin/categories");
    })
});

Router.post("/categories/update", AdminAuth, (req, res) => {
    var vId = req.body.idCategory;
    var vUpdatedTitle = req.body.updatedTitle;

    if((vId != undefined) && (vUpdatedTitle != undefined)){
        if(!isNaN(vId)){
            Category.update({title: vUpdatedTitle, slug: Slugify(vUpdatedTitle)},{
                where: {
                    id: vId
                }
            }).then(() => {
                res.redirect("/admin/categories");
            });
        }else{
            res.redirect("/admin/categories"); // Se id não for um número
        }
    }else{
        res.redirect("/admin/categories"); // Se id for nulo
    }
});


module.exports = Router;