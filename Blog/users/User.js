const Sequelize = require("sequelize");
const dbConn = require("../database/database");

// DEFINIÇÃO DO MODEL DE USUÁRIOS 
const User = dbConn.define('users',{
    name:{
        type: Sequelize.STRING,
        allowNull: false
    },
    email:{
        type: Sequelize.STRING,
        allowNull: false
    },
    password:{
        type: Sequelize.STRING,
        allowNull: false
    }
});

module.exports = User;