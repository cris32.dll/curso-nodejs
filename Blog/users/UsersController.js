const Express = require("express");
const Router = Express.Router();
const Bcrypt = require("bcryptjs");
const User = require("./User");
const AdminAuth = require("../middlewares/adminAuth");

Router.get("/admin/users", AdminAuth, (req, res) => {
    User.findAll().then(u => {
    res.render("admin/users/index", {users: u});
    });
});

Router.get("/admin/users/create", AdminAuth, (req, res) => {
    res.render("admin/users/create");
});

Router.post("/users/create", AdminAuth, (req, res) => {
    var vName = req.body.txtName;
    var vEmail = req.body.txtEmail;
    var vPassword = req.body.txtPassword;

    User.findOne({
        where: {
            email: vEmail
        }
    }).then( u => {
        if(u == undefined) {
            //Fazendo o hash da senha
            var vSalt = Bcrypt.genSaltSync(10);
            var vHash = Bcrypt.hashSync(vPassword, vSalt);

            User.create({
                name: vName,
                email: vEmail,
                password: vHash
            }).then(() => {
                res.redirect("/");
            }).catch((err) => {
                res.redirect("/");
            });
        }else {
            res.render("admin/users/create");
        }
    });
});

Router.get("/login", (req, res) => {
    res.render("admin/users/login");
});

Router.post("/authenticate", (req, res) => {
    var vEmail = req.body.txtEmail;
    var vPassword = req.body.txtPassword;

    User.findOne({
        where: {
            email: vEmail
        }
    }).then( u => {
        if(u != undefined) {

            // Validação da senha e criação da sessão
            var passCorrect = Bcrypt.compareSync(vPassword, u.password);
            if(passCorrect){
                req.session.userLogged = {
                    id: u.id,
                    email: u.email
                }
                res.redirect("/admin/articles");
            }else{
                res.redirect("/login");
            }
        }else {
            res.redirect("/login");
        }
    });
});

Router.get("/logout", (req, res) => {
    req.session.userLogged = undefined;
    res.redirect("/");
})

module.exports = Router;