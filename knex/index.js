var database = require("./database");

// Where - Existem outros tipos de where - Where() , orWhere()
database.select()
    .whereRaw("price > 100")
    .table("games").then(d => {
        console.log(d);
    }).catch(err => {
        console.log(err);
 });

//Delite
database.where({id: 3}).delete().table("games").then(d => {
    console.log(d);
}).catch(err => {
    console.log(err);
});


//Update
database.where({id: 3}).update({price: 65}).table("games").then(d => {
    console.log(d);
}).catch(err => {
    console.log(err);
});
