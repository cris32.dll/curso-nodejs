const versaoCalc = "1.0.0";

function soma(a, b) {
    return a + b;
}

function mult(a, b) {
    return a * b;
}

function sub(a, b) {
    return a - b;
}

function div(a, b) {
    if(b != 0) {
        return a / b;
    }else {
        console.log("Indefinido");
        return undefined;
    }
}

module.exports = {
    soma, 
    mult, 
    sub, 
    div,
    versaoCalc
};
 