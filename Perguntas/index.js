//Tecnologia usada:
//Express     - framework para manipulações http
//MySql       - Banco de dados
//ESJ         - Motor de templante para exibir e manipular html
//body-parser - Feramenta de manipulação do formulario enviado. Vem junto com o Express

const express = require("express");
const app = express();
const dbConn = require("./database/database");
const perguntaModel = require("./database/Pergunta");
const respostaModel = require("./database/Resposta");

//Database
dbConn
    .authenticate()
    .then(() => {
        console.log("Conectou com o banco de dados.")
    })
    .catch((msgErro) => {
        console.log(msgErro)
    })

//Configura o express para usar o ejs como view engine.
app.set('view engine', 'ejs');
//Configura o express para usar arquivos estaticos na pasta public - por padrão o nome desta pasta é "public"
app.use(express.static('public'));
//Configura o express para usar o body-parser.
app.use(express.urlencoded({ extended: true })); 
app.use(express.json());

//Rotas

//Principal
app.get("/",(req, res) => {
    perguntaModel.findAll({ raw: true, order:[
        ['id','DESC'] //Ordenação: ASC = Crescente | DESC = Descrescente
    ]}).then(p => {
        res.render("index", {
            lstPerguntas: p
        });
    });
});

//Cadastro de Perguntas
app.get("/cadastrar-pergunta", (req, res) => {
    res.render("cadastro-pergunta");
});

//Rota que receber o cadastro das perguntas
app.post("/salvarPergunta", (req, res) => {
    var titPergunta = req.body.txtTitPergunta;
    var descPergunta = req.body.memDesPergunta;

    //Insere os dados recebidos no banco de dados
    perguntaModel.create({
        titulo: titPergunta,
        descricao: descPergunta
    }).then(() => {
        res.redirect("/");
    }).catch((msgErro) => {
        console.log("Vix...Deu ruim!!!");
    });
});

//Rota de respostas
app.get("/pergunta/:pId", (req, res) => {
    var vId = req.params.pId;

    perguntaModel.findOne({
        where: {id: vId}
    }).then(p => {
        if(p != undefined) { //Se a consulta não encontrar nada, ela retorna um objeto undefined
            
            respostaModel.findAll({
                where: {perguntaId: p.id},
                order: [
                    ['id', 'DESC']
                ]
            }).then(r => {
                res.render("pergunta", {
                    objPergunta: p,
                    objRespostas: r
                });
            });
        }else{
            res.redirect("/");
        }
    })
});

app.post("/salvarResposra", (req, res) => {
    var corpoResposta = req.body.memCorpo;
    var idPergunta = req.body.txtIdPergunta;   

    //Insere os dados recebidos no banco de dados
    respostaModel.create({
        corpo: corpoResposta,
        perguntaId: idPergunta
    }).then(() => {
        res.redirect("/pergunta/" + idPergunta);
    }).catch((msgErro) => {
        console.log("Vix...Deu ruim outra vez!!!");
    });
});

//Roda a aplicação na porta 8080
app.listen(8080,() => {
    console.log("App rodando...")
});