//Crição do model Pergunta. Ele vai criar e representar a tabela no banco de dados.

//Importação das dependências
const Sequelize = require("sequelize");
const dbConn = require("./database");

const Pergunta = dbConn.define('tbPerguntas', {
    titulo:{
        type: Sequelize.STRING,
        allowNull: false
    },
    descricao:{
        type: Sequelize.TEXT,
        allowNull: false
    }
});

//Sincroniza com o banco de dados. O Parâmetro force está configurado para não forçar a criação da
//tabela, caso ela já exista.
Pergunta.sync({force: false}).then(() => {
    console.log("Tabela tbPerguntas criada.")
});

module.exports = Pergunta;