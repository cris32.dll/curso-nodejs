//Crição do model Resposta. Ele vai criar e representar a tabela no banco de dados.

//Importação das dependências
const Sequelize = require("sequelize");
const dbConn = require("./database");

const Resposta = dbConn.define('tbRespostas', {
    corpo:{
        type: Sequelize.TEXT,
        allowNull: false
    },
    perguntaId:{
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

//Sincroniza com o banco de dados. O Parâmetro force está configurado para não forçar a criação da
//tabela, caso ela já exista.
Resposta.sync({force: false}).then(() => {
    console.log("Tabela tbRespostas criada.")
});

module.exports = Resposta;