const fs = require("fs");

fs.readFile("./usuario.json", {encoding: 'utf-8'}, (err, data) => {
    if(err){
        console.log(`Erro ao abrir o arquivo: ${err.message}`);
    }else{
        console.log(`
            Arquivo sem converter
            ------------------------------------
            ${data}
            ------------------------------------
        `);
        let conteudoConvertido = JSON.parse(data);
        console.log(`
            Arquivo convertido
            ------------------------------------
            ${JSON.stringify(conteudoConvertido)}
            ------------------------------------
        `);

        conteudoConvertido.nome = "Nascimento Vieira";
        conteudoConvertido.curso = "React";
        conteudoConvertido.modulo = "Intro";

        fs.writeFile("./usuario.json", JSON.stringify(conteudoConvertido), (err) => {
            console.log("Erro ao savar o arquivo");
        });

        console.log(`
            Arquivo Salvo
            ------------------------------------
            ${JSON.stringify(conteudoConvertido)}
            ------------------------------------
        `);

    }
});