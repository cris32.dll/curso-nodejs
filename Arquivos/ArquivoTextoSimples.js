const fs = require("fs");

//Abrindo e lendo um arquivo de texto
fs.readFile("./remessa.txt", {encoding: 'utf-8'}, (err, data) => {
    if(err){
        console.log(`Erro ao abrir o arquivo: ${err.message}`);
    }else{
        console.log(data);
    }
});

//Escrevendo em um arquivo
fs.writeFile("./remessa.txt","Novo conteúdo no arquivo", (err) => {
    if(err){
        console.log("Erro ao savar o arquivo");
    }
});